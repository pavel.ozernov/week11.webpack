import {validate} from './email-validator.js';

const onload = {
    create(type) {
      switch (type) {
        case 'standard':
          return this.registerSection("Join Our Program", "Submit");
        case 'advanced':
          return this.registerSection("Join Our Advanced Program", "Subscribe to Advanced Program");
        default:
          throw new Error(`Section type ${type} is not supported.`);
      }
    },

registerSection(message, button_text) {
    const existingSection = document.getElementById("app-sect");
 
     // create a new section element
     const newSection = document.createElement('section');
     newSection.classList.add('app-section');
     newSection.classList.add('app-join-programme');
 
     // create a new div element and set its display to flex with column direction
     const flexContainer = document.createElement('div');
     flexContainer.style.display = 'flex';
     flexContainer.style.flexDirection = 'column';
     flexContainer.style.height = '100%';
     flexContainer.style.margin = '100px 0';
     newSection.appendChild(flexContainer);
 
     // create three new child elements to add to the flex container
     const childElement1 = document.createElement('h2');
     const childElement2 = document.createElement('div');
     const childElement3 = document.createElement('div');
 
     // add content to each of the child elements
     childElement1.textContent = message;
     childElement2.innerHTML = 'Sed do eiusmod tempor incididunt' + '<br />' + "ut labore et dolore magna aliqua.";
 
     // create input field and button elements
     const inputField = document.createElement('input');
     inputField.type = 'text';
     inputField.style.width = '400px';
     inputField.style.height = '36px';
     inputField.placeholder = 'Email';
     inputField.style.background = 'rgba(255, 255, 255, 0.15)';
     inputField.style.border = '0';
    
    
     //button text and style reference 
    
     const button = document.createElement('button');
     button.textContent = button_text;
     button.classList.add('app-section__button--submit', 'app-section__button');
     
 
     //prevent default and console log of the input value
     button.addEventListener('click', function(event) {
         event.preventDefault();
         if (validate(inputField.value)) {
          alert(inputField.value)
         }else{
          alert("The email address is incorrect")
         };
         console.log(inputField.value);
     });
 
     // add the input field and button to the childElement3 div
     childElement3.appendChild(inputField);
     childElement3.appendChild(button);
 
     childElement1.classList.add('app-title');
     childElement2.classList.add('app-subtitle');
     
 
     // set the child elements' heights to 33.33%
     childElement1.style.flex = '1 1 0';
     childElement2.style.flex = '1 1 0';
     childElement3.style.flex = '1 1 0';
 
     childElement2.style.marginBottom = '20px';
 
     // add display: flex and justify-content: center to center child elements horizontally
     childElement1.style.display = 'flex';
     childElement1.style.justifyContent = 'center';
     childElement2.style.display = 'flex';
     childElement2.style.justifyContent = 'center';
     childElement3.style.display = 'flex';
     childElement3.style.justifyContent = 'center';
 
 
 
     flexContainer.appendChild(childElement1);
     flexContainer.appendChild(childElement2);
     flexContainer.appendChild(childElement3);
 
     // insert the new section element after the existing section element
     existingSection.insertAdjacentElement('afterend', newSection);
 }
};

 export default onload;