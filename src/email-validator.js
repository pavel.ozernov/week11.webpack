const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com'];


export function validate(email) {
    return /\b(@gmail\.com|@outlook\.com)$/.test(email);
  };