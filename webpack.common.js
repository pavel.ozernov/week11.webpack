const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    mode: 'development',
    entry: {
      main: path.resolve(__dirname, 'src/main.js')
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].[contenthash].js',
      clean: true,
    },
    devtool: 'inline-source-map',
    devServer:{
      static: path.resolve(__dirname, 'dist'),
      port:5000,
      open:true,
      hot: true,
    },

//loaders

    module: {
      rules: [
        {
          test: /\.css$/i,
          use: ["style-loader", "css-loader"],
        },
      ],
    },


//plugins
plugins: [
  new HtmlWebpackPlugin({
    template: 'src/index.html'
  }),
  new CopyPlugin({
  patterns: [
    { from: 'src/assets/images/your-logo-footer.png', to: './assets/images/your-logo-footer.png' },
    { from: 'src//assets/images/your-logo-here.png', to: './assets/images/your-logo-here.png' },
    ],
  }),
],
  };